<?php

/**
* LinkCollector
* 
* Simple Class to collect and share Links 
*
* @author     Moritz Buetzer <moritz@buetzer.bz>
*/
class LinkCollector {
	
	private $db;
	
	/**
	* Constructor
	*/
	public function __construct() {
        $this->db = new SQLite3('database.sqlite');
        $this->db->exec('CREATE TABLE IF NOT EXISTS links (id INTEGER PRIMARY KEY, title TEXT, href TEXT, description TEXT, tstamp DATETIME);');
    }
	
	/**
	* Get Links from DB
	*
	* @return SQLite3Result
	*/
    public function getRecords() {
		$stmt = $this->db->prepare('SELECT * FROM links ORDER BY id DESC;');
		return $stmt->execute();
	}

	/**
	* Submit link from frontend to database
	*
	* @param string $href
	* @return void
	*/    
    public function submit($href) {
    	$urlToFetch = $this->_addHttp($this->db->escapeString($href));
		$html = $this->_fileGetContentsCurl($urlToFetch);
	
		//parsing begins here:
		$doc = new DOMDocument();
		@$doc->loadHTML($html);
		$nodes = $doc->getElementsByTagName('title');
		
		$title = $nodes->item(0)->nodeValue;
		$metas = $doc->getElementsByTagName('meta');
		
		for ($i = 0; $i < $metas->length; $i++) {
		    $meta = $metas->item($i);
		    
		    if($meta->getAttribute('name') == 'description') {
			    $metaDescription = $meta->getAttribute('content');
		    }
	
		    if($meta->getAttribute('name') == 'twitter:description') {
			    $twitterDescription = $meta->getAttribute('content');
		    }
		   
		    if($meta->getAttribute('name') == 'og:description') {
			    $ogDescription = $meta->getAttribute('content');
		    }
		}
		
		if( !empty($metaDescription) ) {
			$description = $metaDescription;	
		} else if( !empty($twitterDescription) ) {
			$description = $twitterDescription;
		} else if( !empty($ogDescription) ) {
			$description = $ogDescription;
		} else {
			$description = '';
		}
		
		$cleanDescription = strip_tags(html_entity_decode($description));
	
		$this->db->exec('INSERT INTO links (title,href,description,tstamp) VALUES ("'.$title.'", "'.$urlToFetch.'", "'.$cleanDescription.'", "'.date('Y-m-d H:i:s').'")');
	
		$this->_redirect();
    }
    
	/**
	* Redirect back to referer
	*
	* @return void
	*/
    private function _redirect() {
	    header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
    }  
    
	/**
	* curl website data
	*
	* @param string $href
	* @return mixed
	*/    
    private function _fileGetContentsCurl($href) {
	    $ch = curl_init();
	
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $href);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	
	    $data = curl_exec($ch);
	    curl_close($ch);
	
	    return $data;
	}
	
	/**
	* add protocol to given url
	*
	* @param string $href
	* @return string
	*/
	private function _addHttp($href) {
	    if (!preg_match("~^(?:f|ht)tps?://~i", $href)) {
	        $href = "http://" . $url;
	    }
	    return $href;
	}
    
}